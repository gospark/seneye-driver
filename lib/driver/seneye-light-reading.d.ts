export declare class SeneyeLightReading {
    readonly Kelvin: number;
    readonly InKelvinRange: boolean;
    readonly XPos: number;
    readonly YPos: number;
    readonly PAR: number;
    readonly LUX: number;
    readonly PUR: number;
    constructor(kelvin: number, inKelvinRange: boolean, xpos: number, ypos: number, par: number, lux: number, pur: number);
}
