import { SeneyeLightReading } from './seneye-light-reading';
import { SeneyeReading } from './seneye-reading';
export interface ISeneyeDriver {
    initialize: () => Promise<void>;
    read: () => Promise<SeneyeReading>;
    readLighting: () => Promise<SeneyeLightReading>;
    close: () => Promise<void>;
}
