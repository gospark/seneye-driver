import { ISeneyeDriver } from './i-seneye-driver';
import { SeneyeLightReading } from './seneye-light-reading';
import { SeneyeReading } from './seneye-reading';
export declare class SeneyeDriverV2 implements ISeneyeDriver {
    private static readonly deviceVID;
    private static readonly devicePID;
    private readonly logger;
    private seneyeDevice?;
    private initialized;
    private attempts;
    private wait;
    constructor(logger: (message: string) => void, attempts: number, wait: number);
    initialize: () => Promise<void>;
    read: () => Promise<SeneyeReading>;
    readLighting: () => Promise<SeneyeLightReading>;
    close: () => Promise<void>;
    private onConnect;
    private connect;
    private sendBye;
    private sendHello;
    private onDisconnect;
    private disconnect;
    private delaySeconds;
    private readFromDevice;
    private writeToDevice;
    private fillBuffer;
    private byteArrayToLong;
}
