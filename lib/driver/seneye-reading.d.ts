export declare class SeneyeReading {
    readonly timestamp: number;
    readonly ph: number;
    readonly nh3: number;
    readonly temperature: number;
    readonly inWater: boolean;
    readonly slideExpired: boolean;
    readonly slideNotFitted: boolean;
    constructor(timestamp: number, ph: number, nh3: number, temperature: number, inWater: boolean, slideExpired: boolean, slideNotFited: boolean);
}
