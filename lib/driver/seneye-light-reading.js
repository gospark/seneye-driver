"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SeneyeLightReading {
    constructor(kelvin, inKelvinRange, xpos, ypos, par, lux, pur) {
        this.Kelvin = kelvin;
        this.InKelvinRange = inKelvinRange;
        this.XPos = xpos;
        this.YPos = ypos;
        this.PAR = par;
        this.LUX = lux;
        this.PUR = pur;
    }
}
exports.SeneyeLightReading = SeneyeLightReading;
//# sourceMappingURL=seneye-light-reading.js.map