"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SeneyeReading {
    constructor(timestamp, ph, nh3, temperature, inWater, slideExpired, slideNotFited) {
        this.timestamp = timestamp;
        this.ph = ph;
        this.nh3 = nh3;
        this.temperature = temperature;
        this.inWater = inWater;
        this.slideExpired = slideExpired;
        this.slideNotFitted = slideNotFited;
    }
}
exports.SeneyeReading = SeneyeReading;
//# sourceMappingURL=seneye-reading.js.map