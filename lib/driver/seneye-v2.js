"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_hid_1 = require("node-hid");
const usbDetect = require("usb-detection");
const seneye_light_reading_1 = require("./seneye-light-reading");
const seneye_reading_1 = require("./seneye-reading");
class SeneyeDriverV2 {
    constructor(logger, attempts, wait) {
        this.initialized = false;
        this.initialize = async () => {
            // set up usb monitoring.  If we cant connect straight away, we just want it to connect
            // when the device becomes available.
            usbDetect.startMonitoring();
            usbDetect.on(`add:${SeneyeDriverV2.deviceVID}:${SeneyeDriverV2.devicePID}`, this.onConnect);
            usbDetect.on(`remove:${SeneyeDriverV2.deviceVID}:${SeneyeDriverV2.devicePID}`, this.onDisconnect);
            await this.connect();
        };
        this.read = async () => {
            if (!this.seneyeDevice || !this.initialized) {
                return Promise.reject('the seneye device wasnt ready.');
            }
            try {
                let attemptsLeft = this.attempts;
                // send the read request
                while (attemptsLeft > 0) {
                    this.logger('sending reading request');
                    this.writeToDevice(this.fillBuffer('READING'));
                    const response = await this.readFromDevice();
                    if (response[0] === 0x88 && response[1] === 0x02) {
                        this.logger('received read request response');
                        if (response[2] !== 0x00) {
                            // the response indicated success.
                            this.logger('reading request was queued sucessfully');
                            break;
                        }
                    }
                    this.logger(`reading request attempt ${this.attempts +
                        1 -
                        attemptsLeft}: recieved incorrect response from reading request. trying again in ${this.wait} seconds.`);
                    await this.delaySeconds(this.wait);
                    attemptsLeft--;
                }
                if (attemptsLeft === 0) {
                    throw new Error(`Failed to request the experiment data from the seneye controller after ${this.attempts} attempts.`);
                }
                attemptsLeft = this.attempts;
                while (attemptsLeft > 0) {
                    const data = await this.readFromDevice();
                    // assert the correct data type:
                    if (data[0] === 0x00 && data[1] === 0x01) {
                        // convert the data to the correct format
                        const timestamp = this.byteArrayToLong(data.slice(0, 4).reverse());
                        const flag1 = data[4];
                        // tslint:disable:no-bitwise
                        const inWater = (flag1 & 4) === 4;
                        const slideNotFitted = (flag1 & 8) === 8;
                        const slideExpired = (flag1 & 16) === 16;
                        const flag2 = data[5];
                        if ((flag2 & 8) === 8) {
                            throw new Error('The seneye device is not able to take readings. Please connect it to the SCA to allow it to sync.');
                        }
                        const ph = this.byteArrayToLong(data.slice(8, 10)) / 100;
                        const nh3 = this.byteArrayToLong(data.slice(10, 12)) / 1000;
                        const temperature = this.byteArrayToLong(data.slice(12, 16)) / 1000;
                        return new seneye_reading_1.SeneyeReading(timestamp, ph, nh3, temperature, inWater, slideExpired, slideNotFitted);
                    }
                    this.logger(`read experiment result attempt ${this.attempts +
                        1 -
                        attemptsLeft}: recieved incorrect response from reading. trying again in ${this.wait} seconds.`);
                    await this.delaySeconds(this.wait);
                    attemptsLeft--;
                }
                throw new Error(`Failed to read experiment data from the seneye controller after ${this.attempts} attempts.`);
            }
            catch (e) {
                this.logger(`Failed reading: ${e}`);
                return Promise.reject();
            }
        };
        this.readLighting = async () => {
            if (!this.seneyeDevice || !this.initialized) {
                return Promise.reject('the seneye device wasnt ready.');
            }
            try {
                await this.sendHello();
                let attemptsLeft = this.attempts;
                while (attemptsLeft > 0) {
                    const data = await this.readFromDevice();
                    // assert the correct data type:
                    if (data[0] === 0x00 && data[1] === 0x02) {
                        // convert the data to the correct format
                        const inKelvinRange = data[0] === 0x00;
                        const kelvin = this.byteArrayToLong(data.slice(12, 16));
                        const xpos = this.byteArrayToLong(data.slice(16, 20)) / 10000;
                        const ypos = this.byteArrayToLong(data.slice(20, 24)) / 10000;
                        const par = this.byteArrayToLong(data.slice(24, 28));
                        const lux = this.byteArrayToLong(data.slice(28 - 32));
                        const pur = data[32];
                        return new seneye_light_reading_1.SeneyeLightReading(kelvin, inKelvinRange, xpos, ypos, par, lux, pur);
                    }
                    this.logger(`read lighting attempt ${this.attempts +
                        1 -
                        attemptsLeft}: recieved incorrect response from read lighting. trying again in ${this.wait} seconds.`);
                    await this.delaySeconds(this.wait);
                    attemptsLeft--;
                }
                throw new Error(`Failed to read the lighting data from the seneye controller after ${this.attempts} attempts.`);
            }
            catch (e) {
                this.logger(`Failed reading lighting results: ${e}`);
                return Promise.reject(`Failed reading lighting results: ${e}`);
            }
        };
        this.close = async () => {
            // turn off monitoring (will release the monitoring process)
            usbDetect.stopMonitoring();
            await this.disconnect();
        };
        this.onConnect = async () => {
            this.logger('detected a seneye controller. Attempting to connect to it');
            await this.connect();
        };
        this.connect = async () => {
            // If the device is already connected throw an error.
            if (this.seneyeDevice) {
                this.logger(`a seneye controller is already open. close the current controller first.`);
                return;
            }
            this.logger(`Atempting to open the seneye controller`);
            try {
                // connect to the usb device.
                this.seneyeDevice = new node_hid_1.HID(SeneyeDriverV2.deviceVID, SeneyeDriverV2.devicePID);
                this.seneyeDevice.setNonBlocking(1);
                // send the hello handshake
                if (await this.sendHello()) {
                    this.logger(`Opened the seneye controller.`);
                }
                else {
                    await this.sendBye();
                    this.seneyeDevice = undefined;
                }
            }
            catch (e) {
                this.logger(`error opening seneye controller: ${e}`);
                this.seneyeDevice = undefined;
            }
        };
        this.sendBye = async () => {
            if (!this.seneyeDevice) {
                return false;
            }
            // try 10 times 10 seconds apart.
            try {
                let attemptsLeft = this.attempts;
                while (attemptsLeft > 0) {
                    this.logger('sending byesud');
                    this.writeToDevice(this.fillBuffer('BYESUD'));
                    const response = await this.readFromDevice();
                    if (response[0] === 0x88 && response[1] === 0x05) {
                        this.logger('received byesud response');
                        if (response[2] === 0x00) {
                            throw new Error(`Failed to default the seneye controller`);
                        }
                        // the response indicated success.
                        this.logger('byesud was sucessful');
                        return true;
                    }
                    this.logger(`byesud attempt ${this.attempts +
                        1 -
                        attemptsLeft}: recieved incorrect response from byesud. trying again in ${this.wait} seconds.`);
                    await this.delaySeconds(this.wait);
                    attemptsLeft--;
                }
                throw new Error(`Failed to default the seneye controller after ${this.attempts} attempts.`);
            }
            catch (e) {
                this.logger(`Failed sending BYESUD: ${e}`);
                return false;
            }
        };
        this.sendHello = async () => {
            if (!this.seneyeDevice || this.initialized) {
                return false;
            }
            try {
                let attemptsLeft = this.attempts;
                while (attemptsLeft > 0) {
                    this.logger('sending hellosud');
                    this.writeToDevice(this.fillBuffer('HELLOSUD'));
                    const response = await this.readFromDevice();
                    // check valid response
                    if (response[0] === 0x88 && response[1] === 0x01) {
                        // check device acknowledged
                        if (response[2] === 0x00) {
                            throw new Error(`Device wont initiate: The device need to connected to a Seneye SCA or a SWS to operate correctly`);
                        }
                        // make sure this is a reef model
                        if (response[3] !== 0x03) {
                            throw new Error(`Only the reef model is supported currently`);
                        }
                        // the response indicated success.
                        this.initialized = true;
                        this.logger('hellosud was sucessful');
                        return true;
                    }
                    this.logger(`hellosud attempt ${this.attempts +
                        1 -
                        attemptsLeft}: recieved incorrect response from hellosud. trying again in ${this.wait} seconds.`);
                    await this.delaySeconds(this.wait);
                    attemptsLeft--;
                }
                throw new Error(`Failed to handshake with the the seneye controller after ${this.attempts} attempts.`);
            }
            catch (e) {
                this.logger(`Failed sending HELLOSUD: ${e}`);
                return false;
            }
        };
        this.onDisconnect = async () => {
            this.logger('detected the seneye controller being disconnected.');
            await this.disconnect();
            this.logger('The seneye controller will automatically attempt to reconnect when the device is available.');
        };
        this.disconnect = async () => {
            if (!this.seneyeDevice) {
                // nothing to disconnect
                return;
            }
            try {
                this.logger('attempting to close the seneye controller');
                await this.sendBye();
                this.seneyeDevice.close();
            }
            catch (e) {
                // we dont really care. there is a good chance the device was disconnected
                // and so we cant close it properly.
                this.logger(`exception when closing the seneye controller.. forcing close instead: ${e}`);
            }
            this.seneyeDevice = undefined;
            this.logger('seneye controller closed');
            this.initialized = false;
        };
        // Helper function to allow delaying execution so we dont smash the
        // seneye with millions of requests.
        this.delaySeconds = (seconds) => {
            return new Promise(res => {
                setTimeout(res, seconds * 1000);
            });
        };
        this.readFromDevice = async () => {
            return new Promise((resolve, reject) => {
                if (!this.seneyeDevice) {
                    reject('seneye device not available');
                }
                this.seneyeDevice.read((error, data) => {
                    if (error) {
                        reject(error);
                    }
                    resolve(data);
                });
            });
        };
        this.writeToDevice = (data) => {
            if (!this.seneyeDevice) {
                return;
            }
            this.seneyeDevice.write(data);
        };
        this.fillBuffer = (data, length = 69) => {
            const buffer = new Array(length);
            buffer.fill(0x00);
            for (let i = 0; i < data.length; i++) {
                buffer[i] = data.charCodeAt(i);
            }
            if (process.platform === 'win32') {
                buffer.unshift(0); // prepend throwaway byte
            }
            return buffer;
        };
        this.byteArrayToLong = (byteArray) => {
            let value = 0;
            for (let i = byteArray.length - 1; i >= 0; i--) {
                value = value * 256 + byteArray[i];
            }
            return value;
        };
        this.logger = logger;
        this.attempts = attempts;
        this.wait = wait;
    }
}
SeneyeDriverV2.deviceVID = 9463;
SeneyeDriverV2.devicePID = 8708;
exports.SeneyeDriverV2 = SeneyeDriverV2;
//# sourceMappingURL=seneye-v2.js.map