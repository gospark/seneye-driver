"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const seneye_v2_1 = require("../driver/seneye-v2");
// tslint:disable:no-console
const seneyeController = new seneye_v2_1.SeneyeDriverV2(console.log, 10, 1);
seneyeController
    .initialize()
    .then(() => console.log('initialized'))
    .then(() => 
// seneyeController
//     .read()
//     .catch(err => {
//         throw new Error(err);
//     })
//     .then(res => console.log('reading:', JSON.stringify(res)))
//     .then(() =>
seneyeController
    .readLighting()
    .catch(err => {
    throw new Error(err);
})
    .then(res => console.log('lighting:', JSON.stringify(res)))
    .then(() => seneyeController.close().catch(err => {
    throw new Error(err);
})));
// );
//# sourceMappingURL=test-harness.js.map