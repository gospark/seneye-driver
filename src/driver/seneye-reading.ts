export class SeneyeReading {
    public readonly timestamp: number; // epoch
    public readonly ph: number;
    public readonly nh3: number; // amonia levels
    public readonly temperature: number; // degrees C
    public readonly inWater: boolean;
    public readonly slideExpired: boolean;
    public readonly slideNotFitted: boolean;

    public constructor(
        timestamp: number,
        ph: number,
        nh3: number,
        temperature: number,
        inWater: boolean,
        slideExpired: boolean,
        slideNotFited: boolean
    ) {
        this.timestamp = timestamp;
        this.ph = ph;
        this.nh3 = nh3;
        this.temperature = temperature;
        this.inWater = inWater;
        this.slideExpired = slideExpired;
        this.slideNotFitted = slideNotFited;
    }
}
