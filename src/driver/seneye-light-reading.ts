export class SeneyeLightReading {
    public readonly Kelvin: number;
    public readonly InKelvinRange: boolean;
    public readonly XPos: number;
    public readonly YPos: number;
    public readonly PAR: number;
    public readonly LUX: number;
    public readonly PUR: number;

    public constructor(
        kelvin: number,
        inKelvinRange: boolean,
        xpos: number,
        ypos: number,
        par: number,
        lux: number,
        pur: number
    ) {
        this.Kelvin = kelvin;
        this.InKelvinRange = inKelvinRange;
        this.XPos = xpos;
        this.YPos = ypos;
        this.PAR = par;
        this.LUX = lux;
        this.PUR = pur;
    }
}
