import { SeneyeDriverV2 } from '../driver/seneye-v2';

// tslint:disable:no-console
const seneyeController = new SeneyeDriverV2(console.log, 10, 1);

seneyeController
    .initialize()
    .then(() => console.log('initialized'))
    .then(() =>
        // seneyeController
        //     .read()
        //     .catch(err => {
        //         throw new Error(err);
        //     })
        //     .then(res => console.log('reading:', JSON.stringify(res)))
        //     .then(() =>
        seneyeController
            .readLighting()
            .catch(err => {
                throw new Error(err);
            })
            .then(res => console.log('lighting:', JSON.stringify(res)))
            .then(() =>
                seneyeController.close().catch(err => {
                    throw new Error(err);
                })
            )
    );
// );
